FROM python

WORKDIR /app
RUN apt update

COPY requirements.txt .
RUN pip install -r requirements.txt  # Install dependencies

COPY . .

EXPOSE 5000

CMD ["python3", "-m","Flask","run","--host=0.0.0.0"] 