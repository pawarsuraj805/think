from flask import Flask, render_template, request, session
from users import User  # Import the User class
from utils import get_current_user

app = Flask(__name__)

# Replace with your actual secret key for session management
app.secret_key = 'your_secret_key'

# Simulate a user data store (replace with database integration later)
users = {'admin': User(username='admin', password='admin')}

@app.route("/")
def home():
    user = get_current_user()
    if user:
        return render_template("home.html", user=user)
    else:
        return render_template("login.html")

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000) 